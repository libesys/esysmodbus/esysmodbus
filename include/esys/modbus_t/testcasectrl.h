/*!
 * \file esys/modbus_t/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esystest/testcasectrl.h>

namespace esys
{

namespace modbus_t
{

class TestCaseCtrl : public esystest::TestCaseCtrl
{
public:
    TestCaseCtrl();
    virtual ~TestCaseCtrl();

    static TestCaseCtrl &Get();

protected:
    static TestCaseCtrl *g_test_case;
};

} // namespace modbus_t

} // namespace esys
