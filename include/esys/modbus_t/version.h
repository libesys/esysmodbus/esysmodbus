/*!
 * \file esysmodbus/version.h
 * \brief Version info for esysmodbus_t
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSMODBUS_T_MAJOR_VERSION    0
#define ESYSMODBUS_T_MINOR_VERSION    0
#define ESYSMODBUS_T_RELEASE_NUMBER   1
#define ESYSMODBUS_T_VERSION_STRING   "esysmodbus_t 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSMODBUS_T_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYSMODBUS_T_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYSMODBUS_T_VERSION_NUMBER (ESYSMODBUS_T_MAJOR_VERSION * 1000) + (ESYSMODBUS_T_MINOR_VERSION * 100) + ESYSMODBUS_T_RELEASE_NUMBER
#define ESYSMODBUS_T_BETA_NUMBER      1
#define ESYSMODBUS_T_VERSION_FLOAT ESYSMODBUS_T_MAJOR_VERSION + (ESYSMODBUS_T_MINOR_VERSION/10.0) + (ESYSMODBUS_T_RELEASE_NUMBER/100.0) + (ESYSMODBUS_T_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYSMODBUS_T_CHECK_VERSION(major,minor,release) \
    (ESYSMODBUS_T_MAJOR_VERSION > (major) || \
    (ESYSMODBUS_T_MAJOR_VERSION == (major) && ESYSMODBUS_T_MINOR_VERSION > (minor)) || \
    (ESYSMODBUS_T_MAJOR_VERSION == (major) && ESYSMODBUS_T_MINOR_VERSION == (minor) && ESYSMODBUS_T_RELEASE_NUMBER >= (release)))

