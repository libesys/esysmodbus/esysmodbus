/*!
 * \file esys/modbus/defs.h
 * \brief Definitions needed for esysmodbus
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSMODBUS_EXPORTS
#define ESYSMODBUS_API __declspec(dllexport)
#elif ESYSMODBUS_USE
#define ESYSMODBUS_API __declspec(dllimport)
#else
#define ESYSMODBUS_API
#endif

#if defined(_MSC_VER) && !defined(ESYSMODBUS_EXPORTS)
#include "esys/modbus/msvc/autolink.h"
#endif
