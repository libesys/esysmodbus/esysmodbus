/*!
 * \file esys/modbus/transceivercallback.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"

#include <inttypes.h>

namespace esys
{

namespace modbus
{

class ESYSMODBUS_API TransceiverBase;

/*! \class RadioBase esyslora/radiocallback.h "esyslora/radiocallback.h"
 *  \brief
 *
 */
class ESYSMODBUS_API TransceiverCallback
{
public:
    enum class Event
    {
        TX_DONE,
        TX_TIMEOUT,
        RX_TIMEOUT,
        RX_ERROR,
    };

    //! Constructor
    TransceiverCallback();

    //! Destructor
    virtual ~TransceiverCallback();

    //! Notification sent by the transceiver
    /*!
     * \param[in] transceiver the transceiver which sends the notification
     * \param[in] evt the event being notified

     */
    virtual void event(TransceiverBase *transceiver, Event evt) = 0;

    //! Notification when a message was received
    /*!
     * \param[in] transceiver the transceiver which sends the notification
     * \param[in] data the buffer with the
     * message received
     * \param[in] size the size of the received message
     */
    virtual void rx_done(TransceiverBase *transceiver, uint8_t *data, uint32_t size) = 0;
};

} // namespace modbus

} // namespace esys
