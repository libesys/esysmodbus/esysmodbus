/*!
 * \file esys/modbus/uartcfg.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"

#include <ostream>

namespace esys
{

namespace modbus
{

/*! \class UartCfg esys/modbus/uartcfg.h "esys/modbus/uartcfg.h"
 *  \brief
 *
 */
class ESYSMODBUS_API UartCfg
{
public:
    enum class DataBitCount
    {
        Seven,
        Eight
    };

    enum class Parity
    {
        None,
        Odd,
        Even
    };

    enum class StopBit
    {
        One,
        Two
    };

    //! Constructor
    UartCfg();

    //! Destructor
    virtual ~UartCfg();

    //! Set the baurate used to send the data
    /*!
     * \param[in] baudrate the baudrate
     */
    void set_baudrate(unsigned int baudrate);

    //! Get the baurate used to send the data
    /*!
     * \result the baurate
     */
    unsigned int get_baudrate() const;

    //! Set the number of bits of the data
    /*!
     * \param[in] data_bit_count the number of bits of the data
     */
    void set_data_bit_count(DataBitCount data_bit_count);

    //! Get the number of bits of the data
    /*!
     * \result the number of bits of the data
     */
    DataBitCount get_data_bit_count() const;

    //! Set the parity
    /*!
     * \param[in] parity the parity
     */
    void set_parity(Parity parity);

    //! Get the parity
    /*!
     * \result the parity
     */
    Parity get_parity() const;

    //! Set the stop bit
    /*!
     * \param[in] parity the parity
     */
    void set_stop_bit(StopBit stop_bit);

    //! Get the stop bit
    /*!
     * \result the stop bit
     */
    StopBit get_stop_bit() const;

    unsigned int get_symbol_bit_count() const;
protected:
    //!< \cond DOXY_IMPL
    unsigned int m_baudrate = 0;
    DataBitCount m_data_bit_count = DataBitCount::Eight;
    Parity m_parity = Parity::Even;
    StopBit m_stop_bit = StopBit::One;
    //!< \endcond
};

} // namespace modbus

} // namespace esys

ESYSMODBUS_API std::ostream &operator<<(std::ostream &os, const esys::modbus::UartCfg *uart_cfg);
ESYSMODBUS_API std::ostream &operator<<(std::ostream &os, const esys::modbus::UartCfg &uart_cfg);
