/*!
 * \file esysmodbus/version.h
 * \brief Version info for esysmodbus
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSMODBUS_MAJOR_VERSION 0
#define ESYSMODBUS_MINOR_VERSION 1
#define ESYSMODBUS_RELEASE_NUMBER 0
#define ESYSMODBUS_VERSION_STRING "ESysModBus 0.1.0"

// Must be updated manually as well each time the version above changes
#define ESYSMODBUS_VERSION_NUM_DOT_STRING "0.1.0"
#define ESYSMODBUS_VERSION_NUM_STRING "000100"

// nothing should be updated below this line when updating the version

#define ESYSMODBUS_VERSION_NUMBER \
    (ESYSMODBUS_MAJOR_VERSION * 1000) + (ESYSMODBUS_MINOR_VERSION * 100) + ESYSMODBUS_RELEASE_NUMBER
#define ESYSMODBUS_BETA_NUMBER 1
#define ESYSMODBUS_VERSION_FLOAT                                                                       \
    ESYSMODBUS_MAJOR_VERSION + (ESYSMODBUS_MINOR_VERSION / 10.0) + (ESYSMODBUS_RELEASE_NUMBER / 100.0) \
        + (ESYSMODBUS_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSMODBUS_CHECK_VERSION(major, minor, release)                                                                \
    (ESYSMODBUS_MAJOR_VERSION > (major) || (ESYSMODBUS_MAJOR_VERSION == (major) && ESYSMODBUS_MINOR_VERSION > (minor)) \
     || (ESYSMODBUS_MAJOR_VERSION == (major) && ESYSMODBUS_MINOR_VERSION == (minor)                                    \
         && ESYSMODBUS_RELEASE_NUMBER >= (release)))
