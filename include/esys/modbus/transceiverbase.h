/*!
 * \file esys/modbus/transceiverbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/uartcfg.h"
#include "esys/modbus/transceivercallback.h"

namespace esys
{

namespace modbus
{

/*! \class TransceiverBase esys/modbus/transceiverbase.h "esys/modbus/transceiverbase.h"
 *  \brief
 *
 */
class ESYSMODBUS_API TransceiverBase
{
public:
    enum class Status
    {
        IDLE,
        RX_RUNNING,
        TX_RUNNING,
    };

    //! Constructor
    TransceiverBase();

    //! Destructor
    virtual ~TransceiverBase();

    //! Set the uart configuration
    /*!
     * \param[in] uart_cfg the uart configuration of the transceiver
     */
    virtual int set_uart_cfg(const UartCfg &uart_cfg) = 0;

    //! Get the uart configuration of the transceiver
    /*!
     * \return the uart configuration of the transceiver
     */
    virtual const UartCfg &get_uart_cfg() const = 0;

    //! Get the uart configuration of the transceiver
    /*!
     * \return the LoRa configuration of the transceiver
     */
    virtual UartCfg &get_uart_cfg() = 0;

    //! Get the status of the transceiver
    /*!
     * \return the status of the transceiver
     */
    virtual Status get_status() const = 0;

    //! Transmit a message
    /*!
     * \param[in] msg the message to transmit
     * \param[in] size the size of the message
     * \return 0 if
     * success, < 0 otherwise
     */
    virtual int tx(uint8_t *msg, uint32_t size) = 0;

    //! Set the transceiver is receiving mode
    /*!
     * \param[in] timeout the time the receiving mode should be enabled
     * \return 0 if success, < 0
     * otherwise
     */
    virtual int rx(uint32_t timeout) = 0;

    //! Set the callback
    /*!
     * \param[in] callback the callback
     */
    void set_callback(TransceiverCallback *callback);

    //! Get the callback
    /*!
     * \return the callback
     */
    TransceiverCallback *get_callback() const;

    //! Set the ID of the transceiver
    /*!
     * \param[in] id the ID of the transceiver
     */
    void set_id(int id);

    //! Get the ID of the transceiver
    /*!
     * \return the ID of the transceiver
     */
    int get_id() const;

protected:
    //!< \cond DOXY_IMPL
    TransceiverCallback *m_callback = nullptr; //!< The callback
    int m_id = -1;                             //!< The ID of the radio
    //!< \endcond
};

} // namespace modbus

} // namespace esys
