/*!
 * \file esys/modbus/sysc/txtime.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/txtime.h"
#include "esys/modbus/sysc/msg.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class TxTime esys/modbus/sysc/txtime.h "esys/modbus/sysc/txtime.h"
 *  \brief Class used to calculate the transmit time of a modbus msg
 *
 */
class ESYSMODBUS_API TxTime : public modbus::TxTime
{
public:

    using modbus::TxTime::TxTime;

    //! Default constructor
    TxTime();

    //! Constructor
    TxTime(Msg *msg);

    //! Destructor
    virtual ~TxTime();

    void set_msg(Msg *msg);

    Msg *get_msg() const;

    using modbus::TxTime::get_msg_time;

    //! Get the time on air of the packet in sec with a given payload size
    /*!
     * \return the time on air of the packet in sec
     */
    double get_msg_time();

protected:
    //!< \cond DOXY_IMPL
    Msg *m_msg = nullptr;
    //!< \endcond
};

} // namespace esysc

} // namespace modbus

} // namespace esys
