/*!
 * \file esys/modbus/sysc/channel.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/msg_if.h"
#include "esys/modbus/sysc/channel_if.h"
#include "esys/modbus/sysc/channelport.h"
#include "esys/modbus/sysc/transceiver.h"
#include "esys/modbus/sysc/msgqueue.h"

#include <systemc.h>

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class Channel esys/modbus/sysc/channel.h "esys/modbus/sysc/channel.h"
 *  \brief
 *
 */
class ESYSMODBUS_API Channel : public sc_module, public Channel_if
{
public:
    SC_HAS_PROCESS(Channel);

    //! Constructor
    Channel(const sc_module_name &name);

    //! Destructor
    virtual ~Channel();

    //! Add a transceiver to the channel
    /*!
     * \param[in] transceiver the Transceiver added to the channel
     */
    void add(Transceiver *transceiver);

    //! Get all channel ports
    /*!
     * \return all channel ports
     */
    std::vector<std::shared_ptr<ChannelPort>> &get_ports();

    //! Get all channel ports
    /*!
     * \return all channel ports
     */
    const std::vector<std::shared_ptr<ChannelPort>> &get_ports() const;

    // Channel_if
    virtual void ch_tx_msg(std::shared_ptr<ChannelMsg> msg) override;

    //! The main thread of the channel
    void main();

protected:
    //!< \cond DOXY_IMPL
    std::vector<std::shared_ptr<ChannelPort>> m_ports; //!< All the channel ports
    MsgQueue<ChannelMsg> m_msg_queue;                  //!< The queue of messages currently on the air
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
