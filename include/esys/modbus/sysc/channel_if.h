/*!
 * \file esys/modbus/sysc/channel_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/channelmsg.h"
#include "esys/modbus/sysc/channelcb_if.h"

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class Channel_if esys/modbus/sysc/channel_if.h "esys/modbus/sysc/channel_if.h"
 *  \brief
 *
 */
class ESYSMODBUS_API Channel_if
{
public:
    //! Constructor
    Channel_if();

    //! Desstructor
    virtual ~Channel_if();

    //! A ChannelMsg is transmitted
    /*!
     * \param[in] msg the message
     */
    virtual void ch_tx_msg(std::shared_ptr<ChannelMsg> msg) = 0;

    //! Set the callback interface
    /*!
     * \param[in] cb_if the callback interface
     */
    void set_cb_if(ChannelCb_if *cb_if);

    //! Get the callback interface
    /*!
     * \return the callback interface
     */
    ChannelCb_if *get_cb_if();

protected:
    //!< \cond DOXY_IMPL
    ChannelCb_if *m_cb_if = nullptr; //!< The callback interface
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
