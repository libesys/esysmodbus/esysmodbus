/*!
 * \file esys/modbus/sysc/channelmsg.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/msg.h"

#include <systemc.h>

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class ChannelMsg esys/modbus/sysc/channelmsg.h "esys/modbus/sysc/channelmsg.h"
 *  \brief
 *
 */
class ESYSMODBUS_API ChannelMsg
{
public:
    //! Constructor
    ChannelMsg();

    //! Destructor
    virtual ~ChannelMsg();

    //! Set the Msg
    /*!
     * \param[in] msg the rMsg
     */
    void set_msg(std::shared_ptr<Msg> msg);

    //! Get the Msg
    /*!
     * \return the Msg
     */
    std::shared_ptr<Msg> get_msg();

    //! Set the ID of the ChannelPort where the message was sent from
    /*!
     * \param[in] src_port_id the ID of the ChannelPort
     */
    void set_src_port_id(std::size_t src_port_id);

    //! Get the ID of the ChannelPort where the message was sent from
    /*!
     * \return the ID of the ChannelPort
     */
    std::size_t get_src_port_id() const;

    //! Set the time when the transmission of the message started
    /*!
     * \param[in] start_time the start time of the transmission
     */
    void set_start_time(const sc_time &start_time);

    //! Get the time when the transmission of the message started
    /*!
     * \return the start time of the transmission
     */
    const sc_time &get_start_time() const;

    //! Get the time when the transmission of the message started
    /*!
     * \return the start time of the transmission
     */
    sc_time &get_start_time();

    //! Set the time when the transmission of the message ended
    /*!
     * \param[in] end_time the end time of the transmission
     */
    void set_end_time(const sc_time &end_time);

    //! Get the time when the transmission of the message ended
    /*!
     * \return the end time of the transmission
     */
    const sc_time &get_end_time() const;

    //! Get the time when the transmission of the message ended
    /*!
     * \return the end time of the transmission
     */
    sc_time &get_end_time();

    //! Get the ID of the message, which is unique
    /*!
     * \return the ID of the message
     */
    int64_t get_id() const;

protected:
    //!< \cond DOXY_IMPL
    static int64_t m_count; //!< Counts the number of messages created

    int64_t m_id = -1;             //!< The value of m_count when this message was created
    std::shared_ptr<Msg> m_msg;    //!< The Msg
    std::size_t m_src_port_id = 0; //!< The ID of the ChannelPort where the message was sent
    sc_time m_start_time;          //!< Time when the transmission started
    sc_time m_end_time;            //!< Time when the transmission ended
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
