/*!
 * \file esys/modbus/sysc/transceiver.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/transceiverbase.h"
#include "esys/modbus/transceivercallback.h"
#include "esys/modbus/sysc/msg_if.h"

#include <systemc.h>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class RadioBase esys/modbus/sysc/transceiver.h "esys/modbus/sysc/transceiver.h"
 *  \brief
 *
 */
class ESYSMODBUS_API Transceiver : public sc_module, public MsgCb_if, public TransceiverBase
{
public:
    SC_HAS_PROCESS(Transceiver);

    enum class InternalState
    {
        IDLE,
        GOING_TO_RX,
        RX,
        GOING_TO_IDLE,
        GOING_TO_TX,
        TX,
        GOING_TX_TO_IDLE
    };

    //! Default constructor
    Transceiver();

    //! Constructor
    /*!
     * \param[in] name the name of the SX1272
     */
    Transceiver(const sc_module_name &name);

    //! Destructor
    virtual ~Transceiver();

    //! Set the Msg interface
    /*!
     * \param[in] msg_if the Msg interface
     */
    void set_msg_if(Msg_if *msg_if);

    //! Get the Msg interface
    /*!
     * \return the Msg interface
     */
    Msg_if *get_msg_if();

    virtual int set_uart_cfg(const UartCfg &uart_cfg) override;
    virtual const UartCfg &get_uart_cfg() const override;
    virtual UartCfg &get_uart_cfg() override;

    virtual Status get_status() const override;

    virtual int tx(uint8_t *packet, uint32_t size) override;

    virtual int rx(uint32_t timeout) override;

    void set_internal_state(InternalState internal_state);
    InternalState get_internal_state();

    void set_rx_timed_out(bool rx_timed_out);
    bool get_rx_timed_out() const;

    // MsgCb_if
    virtual void rx_msg(std::shared_ptr<Msg> msg) override;
    virtual void tx_done() override;

    void main();

protected:
    void set_status(Status status);

    UartCfg m_uart_cfg;         //!< Uart configuration
    Msg_if *m_msg_if = nullptr; //!< The Msg interface
    Status m_status = Status::IDLE;
    int8_t m_tx_power = 0;
    double m_frequency = 0.0;

    std::shared_ptr<Msg> m_rx_msg;
    std::shared_ptr<Msg> m_tx_msg;
    InternalState m_internal_state = InternalState::IDLE;
    bool m_rx_timed_out = false;

    sc_event m_next_internal_state_evt;

    sc_time m_rx_timeout;

    sc_time m_idle_to_rx{100.0, SC_NS};
    sc_time m_rx_to_idle{100.0, SC_NS};
    sc_time m_idle_to_tx{100.0, SC_NS};
    sc_time m_tx_to_idle{100.0, SC_NS};
};

} // namespace sysc

} // namespace modbus

} // namespace esys
