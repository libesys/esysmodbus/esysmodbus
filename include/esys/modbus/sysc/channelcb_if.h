/*!
 * \file esys/modbus/sysc/channelcb_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/channelmsg.h"

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class ChannelCb_if esys/modbus/sysc/channelcb_if.h "esys/modbus/sysc/channelcb_if.h"
 *  \brief
 *
 */
class ESYSMODBUS_API ChannelCb_if
{
public:
    //! Constructor
    ChannelCb_if();

    //! Destructor
    virtual ~ChannelCb_if();

    //! Indicates that the reception of a message has started
    /*!
     * \param[in] msg the message being currently being received
     */
    virtual void chcb_msg_start(std::shared_ptr<ChannelMsg> msg) = 0;

    //! Indicates that the reception of a message has completed
    /*!
     * \param[in] msg the received message
     */
    virtual void chcb_msg_done(std::shared_ptr<ChannelMsg> msg) = 0;
};

} // namespace sysc

} // namespace modbus

} // namespace esys
