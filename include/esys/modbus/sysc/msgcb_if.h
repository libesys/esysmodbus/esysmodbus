/*!
 * \file esys/modbus/sysc/msgcb_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/msg.h"

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class MsgCb_if esys/modbus/sysc/msgcb_if.h "esys/modbus/sysc/msgcb_if.h"
 *  \brief
 *
 */
class ESYSMODBUS_API MsgCb_if
{
public:
    //! Constructor
    MsgCb_if();

    //! Destructor
    virtual ~MsgCb_if();

    //! A message was received
    /*!
     * \param[in] msg the received message
     */
    virtual void rx_msg(std::shared_ptr<Msg> msg) = 0;

    //! The transfer of the message is done
    /*!
     */
    virtual void tx_done() = 0;
};

} // namespace sysc

} // namespace modbus

} // namespace esys
