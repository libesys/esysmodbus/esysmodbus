/*!
 * \file esys/modbus/sysc/msg_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/msg.h"
#include "esys/modbus/sysc/msgcb_if.h"

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class Msg_if esys/modbus/sysc/msg_if.h "esys/modbus/sysc/msg_if.h"
 *  \brief
 *
 */
class ESYSMODBUS_API Msg_if
{
public:
    //! Constructor
    Msg_if();

    //! Destructor
    virtual ~Msg_if();

    //! Transmit a message
    /*!
     * \param[in] msg the message to transmit
     */
    virtual void tx_msg(std::shared_ptr<Msg> msg) = 0;

    //! The radio was setup to receive a message with a given LoRa HW configuration
    /*!
     * \param[in] lora_cfg the LoRa configuration
     */
    virtual void rx(const UartCfg &uart_cfg) = 0;

    //! The radio was setup to idle state
    /*!
     */
    virtual void idle() = 0;

    //! Set the callback interface to use
    /*!
     * \param[in] cb_if the callback interface
     */
    void set_cb_if(MsgCb_if *cb_if);

    //! Get the callback interface to use
    /*!
     * \return the callback interface
     */
    MsgCb_if *get_cb_if();

protected:
    //!< \cond DOXY_IMPL
    MsgCb_if *m_cb_if = nullptr; //!< the callback interface
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
