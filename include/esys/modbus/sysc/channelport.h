/*!
 * \file esys/mdobus/sysc/channelport.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/sysc/msg_if.h"
#include "esys/modbus/sysc/msgcb_if.h"
#include "esys/modbus/sysc/channelmsg.h"
#include "esys/modbus/sysc/channel_if.h"
#include "esys/modbus/sysc/channelcb_if.h"

#include <systemc.h>

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class ChannelPort esys/mdobus/sysc/channelport.h "esys/mdobus/sysc/channelport.h"
 *  \brief
 *
 */
class ESYSMODBUS_API ChannelPort : public sc_module, public Msg_if, public ChannelCb_if
{
public:
    //! Constructor
    ChannelPort(const sc_module_name &name);

    //! Destructor
    virtual ~ChannelPort();

    // Msg_if
    virtual void tx_msg(std::shared_ptr<Msg> msg) override;
    virtual void rx(const UartCfg &uart_cfg) override;
    virtual void idle() override;

    // ChannelCb_if
    virtual void chcb_msg_start(std::shared_ptr<ChannelMsg> msg) override;
    virtual void chcb_msg_done(std::shared_ptr<ChannelMsg> msg) override;

    //! Set the ID of the ChannelPort
    /*!
     * \param[in] id the ID of the ChannelPort
     */
    void set_id(std::size_t id);

    //! Get the ID of the ChannelPort
    /*!
     * \return the ID of the ChannelPort
     */
    std::size_t get_id() const;

    //! Set the Channel interface
    /*!
     * \param[in] channel_if the Channel interface
     */
    void set_channel_if(Channel_if *channel_if);

    //! Get the Channel interface
    /*!
     * \return the Channel interface
     */
    Channel_if *get_channel_if() const;

    //! Set the Msg callback interface
    /*!
     * \param[in] msg_cb_if the AirMsg callback interface
     */
    void set_msg_cb_if(MsgCb_if *msg_cb_if);

    //! Get the Msg callback interface
    /*!
     * \return the Msg callback interface
     */
    MsgCb_if *get_msg_cb_if() const;

    //! Set the current ongoing message, which could be received
    /*!
     * \param[in] cur_msg the current ongoing message
     */
    void set_cur_msg(std::shared_ptr<ChannelMsg> cur_msg);

    //! Get the current ongoing message, which could be received
    /*!
     * \return the current ongoing message
     */
    std::shared_ptr<ChannelMsg> get_cur_msg();

    //! Get the current ongoing message, which could be received
    /*!
     * \return the current ongoing message
     */
    const std::shared_ptr<ChannelMsg> get_cur_msg() const;

    //! Get the uart configuration in effect at the moment
    /*!
     * \return the uart configuration in effect at the moment
     */
    const UartCfg *get_uart_cfg();

protected:
    //!< \cond DOXY_IMPL
    std::size_t m_id = 0;                  //!< The ID
    Channel_if *m_channel_if = nullptr;    //!< The Channel interface
    MsgCb_if *m_msg_cb_if = nullptr;       //!< The AirMsg callback interface
    std::shared_ptr<ChannelMsg> m_cur_msg; //!< The current ongoing receiveable message
    const UartCfg *m_uart_cfg = nullptr;   //!< The LoRa configuration is used to receive a message
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
