/*!
 * \file esys/modbus/sysc/msgqueue.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <systemc.h>

#include <deque>
#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class MsgQueue esys/modbus/sysc/msgqueue.h "esys/modbus/sysc/msgqueue.h"
 *  \brief
 *
 */
template<typename MSG>
class MsgQueue
{
public:
    struct Item
    {
        std::shared_ptr<MSG> m_msg;
    };

    //! Constructor
    MsgQueue();

    //! Add a message to the queue
    /*!
     * \param[in] msg the message to add to the queue
     */
    void add(std::shared_ptr<MSG> msg);

    //! Remove the message which was transmitted
    /*!
     * \param[in] update_event_notif if true, the next message to be transmitted is calculated
     */
    void pop(bool update_event_notif = false);

    //! Set if the message are sorted by end time or start time.
    /*!
     * \param[in] sort_by_end_time if true, messages are sorted by end time, otherwise by start time
     */
    void set_sort_by_end_time(bool sort_by_end_time = true);

    //! Tells if the message are sorted by end time or start time.
    /*!
     * \return if true, messages are sorted by end time, otherwise by start time
     */
    bool get_sort_by_end_time();

    //! The event used to notify that a message was processed
    /*!
     * \return the event
     */
    sc_event &get_event();

    //! Return the structure holding the Message in the queue identified by its index
    /*!
     * \param[in] idx the index of the message in the queue
     * \return the event
     */
    Item &operator[](std::size_t idx);

    //! Return the structure holding the Message in the queue identified by its index
    /*!
     * \param[in] idx the index of the message in the queue
     * \return the event
     */
    const Item &operator[](std::size_t idx) const;

    //! Return the size of the queue
    /*!
     * \return the size of the queue
     */
    std::size_t size();

    //! Update the event triggered when a message transmission is complete
    /*!
     */
    void update_event();

protected:
    //!< \cond DOXY_IMPL
    std::deque<Item> m_items;       //!< The queue of messages
    bool m_sort_by_end_time = true; //!< Tell if messages are sorted by end time or start time
    sc_event m_event;               //!< The event triggered when the transmission of a message is completed
    //!< \endcond
};

template<typename MSG>
MsgQueue<MSG>::MsgQueue()
{
}

template<typename MSG>
void MsgQueue<MSG>::add(std::shared_ptr<MSG> msg)
{
    Item item;

    item.m_msg = msg;

    m_items.push_back(item);

    struct
    {
        bool operator()(Item &a, Item &b) const
        {
            return a.m_msg->get_end_time() < b.m_msg->get_end_time();
        }
    } sort_by_end_time;

    struct
    {
        bool operator()(Item &a, Item &b) const
        {
            return a.m_msg->get_start_time() < b.m_msg->get_start_time();
        }
    } sort_by_start_time;

    if (get_sort_by_end_time())
        std::sort(m_items.begin(), m_items.end(), sort_by_end_time);
    else
        std::sort(m_items.begin(), m_items.end(), sort_by_start_time);

    update_event();
}

template<typename MSG>
void MsgQueue<MSG>::pop(bool update_event_notif)
{
    m_items.pop_front();

    if (update_event_notif) update_event();
}

template<typename MSG>
void MsgQueue<MSG>::update_event()
{
    m_event.cancel();

    if (m_items.size() == 0) return;

    Item &item_ref = m_items[0];

    sc_time now = sc_time_stamp();
    sc_time delta_time;

    if (get_sort_by_end_time())
        delta_time = item_ref.m_msg->get_end_time() - now;
    else
        delta_time = item_ref.m_msg->get_start_time() - now;

    m_event.notify(delta_time);
}

template<typename MSG>
void MsgQueue<MSG>::set_sort_by_end_time(bool sort_by_end_time)
{
    m_sort_by_end_time = sort_by_end_time;
}

template<typename MSG>
bool MsgQueue<MSG>::get_sort_by_end_time()
{
    return m_sort_by_end_time;
}

template<typename MSG>
sc_event &MsgQueue<MSG>::get_event()
{
    return m_event;
}

template<typename MSG>
typename MsgQueue<MSG>::Item &MsgQueue<MSG>::operator[](std::size_t idx)
{
    assert(idx < size());

    return m_items[idx];
}

template<typename MSG>
const typename MsgQueue<MSG>::Item &MsgQueue<MSG>::operator[](std::size_t idx) const
{
    assert(idx < size());

    return m_items[idx];
}

template<typename MSG>
std::size_t MsgQueue<MSG>::size()
{
    return m_items.size();
}

} // namespace sim

} // namespace esyslora

}
