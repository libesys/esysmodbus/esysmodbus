/*!
 * \file esys/modbus/sysc/msg.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/uartcfg.h"

#include <systemc.h>

#include <vector>

namespace esys
{

namespace modbus
{

namespace sysc
{

/*! \class Msg esys/modbus/sysc/msg.h "esys/modbus/sysc/msg.h"
 *  \brief Base class for message send over the air
 *
 */
class ESYSMODBUS_API Msg
{
public:
    //! Constructor
    Msg();

    //! Destructor
    virtual ~Msg();

    //! Set the uart HW configuration used for this message
    /*!
     * \param[in] uarrt_cfg the uart HW configuration
     */
    void set_uart_cfg(const UartCfg &uart_cfg);

    //! Get the uart HW configuration used for this message
    /*!
     * \result the uart HW configuration
     */
    const UartCfg &get_uart_cfg() const;

    //! Get the uart HW configuration used for this message
    /*!
     * \result the uart HW configuration
     */
    UartCfg &get_uart_cfg();

    //! Set the packet, the data sent over the air
    /*!
     * \param[in] data the packet sent over the air
     */
    void set_data(const std::vector<uint8_t> &data);

    //! Get the packet, the data sent over the air
    /*!
     * \return the packet sent over the air
     */
    const std::vector<uint8_t> &get_data() const;

    //! Get the packet, the data sent over the air
    /*!
     * \return the packet sent over the air
     */
    std::vector<uint8_t> &get_data();

    //! Set the size of the packet, the data sent over the air
    /*!
     * \param[in] size the size packet sent over the air
     */
    void set_size(std::size_t size);

    //! Get the size of the packet, the data sent over the air
    /*!
     * \return the size packet sent over the air
     */
    std::size_t get_size() const;

    //! Set the time at which the packet was sent
    /*!
     * \param[in] sent_time the time at which the packet was sentr
     */
    void set_sent_time(const sc_time &sent_time);

    //! Get the time at which the packet was sent
    /*!
     * \return the time at which the packet was sentr
     */
    const sc_time &get_sent_time() const;

    //! Get the ID of the message, which is unique
    /*!
     * \return the ID of the message
     */
    int64_t get_id() const;

protected:
    //!< \cond DOXY_IMPL
    static int64_t m_count; //!< The number of messages created so far

    int64_t m_id = -1;           //!< The ID of the message, the count value when created
    std::vector<uint8_t> m_data; //!< The packet/data send over the air
    UartCfg m_uart_cfg;          //!< Uart configuration
    sc_time m_sent_time;
    //!< \endcond
};

} // namespace sysc

} // namespace modbus

} // namespace esys
