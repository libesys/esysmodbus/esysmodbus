/*!
 * \file esys/modbus/txtime.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/defs.h"
#include "esys/modbus/uartcfg.h"

namespace esys
{

namespace modbus
{

/*! \class TxTime esys/modbus/txtime.h "esys/modbus/txtime.h"
 *  \brief Class used to calculate the transmit time of a modbus msg
 *
 */
class ESYSMODBUS_API TxTime
{
public:
    //! Default Constructor
    TxTime();

    //! Constructor
    TxTime(UartCfg *uart_cfg);

    //! Destructor
    virtual ~TxTime();

    //! Set uart configuration
    /*!
     * \param[in] uart_cfg the uart configuration
     */
    void set_uart_cfg(UartCfg *uart_cfg);

    //! Get uart configuration
    /*!
     * \return the uart configuration
     */
    UartCfg *get_uart_cfg() const;

    //! Get the time on air of the packet in sec with a given payload size
    /*!
     * \return the time on air of the packet in sec
     */
    double get_msg_time(int msg_size);

protected:
    //!< \cond DOXY_IMPL
    UartCfg *m_uart_cfg = nullptr;
    //!< \endcond
};

} // namespace modbus

} // namespace esys
