/*!
 * \file esys/modbus_t/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus_t/esysmodbus_t_prec.h"
#include "esys/modbus_t/testcasectrl.h"

#include <cassert>

namespace esys
{

namespace modbus_t
{

TestCaseCtrl *TestCaseCtrl::g_test_case = nullptr;

TestCaseCtrl &TestCaseCtrl::Get()
{
    assert(g_test_case != nullptr);

    return *g_test_case;
}

TestCaseCtrl::TestCaseCtrl()
    : esystest::TestCaseCtrl()
{
    g_test_case = this;

    AddSearchPathEnvVar("ESYSMODBUS");
    AddSearchPath("res/esysmodbus_t");                      // If cwd is root of the emdev git repo
    AddSearchPath("../../src/esysmodbus/res/esysmodbus_t"); // if cwd is the bin folder
}

TestCaseCtrl::~TestCaseCtrl()
{
}

} // namespace modbus_t

} // namespace esys
