/*!
 * \file esys/modbus_t/sysc/channel01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus_t/esysmodbus_t_prec.h"

#include <esys/modbus/sysc/channel.h>
#include <esys/modbus/sysc/transceiver.h>
#include <esys/modbus/txtime.h>

#include <sysc/sc_simulation.h>
#include <systemc.h>

#include <map>

namespace esys
{

namespace modbus_t
{

namespace sysc
{

namespace channel01
{

class simulation : public sc_simulation
{
public:
    simulation();

    virtual int main() override;

protected:
};

simulation::simulation()
{
}

int simulation::main()
{
    sc_start(10000, SC_MS);

    return 0;
}

const static uint32_t RX_WAIT_MS = 50;
const static double RX_CYCLE_MS = 51.0;

/*! \class Test esys/modbus_t/sysc/channel01.cpp "esys/modbus_t/sysc/channel01.cpp"
 *  \brief
 *
 */
class Test : public sc_module, public modbus::TransceiverCallback
{
public:
    SC_HAS_PROCESS(Test);

    const static int RXTX_COUNT = 3;
    
    //! Constructor
    Test(const sc_module_name &name);

    //! Destructor
    virtual ~Test();

    // esyslora::RadioCallback
    virtual void event(modbus::TransceiverBase *transceiver, Event evt) override;
    virtual void rx_done(modbus::TransceiverBase *transceiver, uint8_t *data, uint32_t size) override;

    // Main thread
    void main();

    //! Add a received event from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which sent the event
     * \param[in] evt the Event
     */
    void add_event(int radio_id, modbus::TransceiverCallback::Event evt);

    //! Get the number of a given Event received from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio of interest
     * \param[in] evt the Event
     * \return the number of times this Event was received from the given radio
     */
    int get_event_count(int radio_id, modbus::TransceiverCallback::Event evt) const;

    //! Add a received message from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which received the message
     * \param[in] data the buffer
     * holding the message
     * \paran[in] size the size of the message in bytes
     */
    void add_receive_msg(int radio_id, uint8_t *data, uint32_t size);

    //! Get all received message from a given Radio
    /*!
     * \param[in] radio_id the ID of the radio which received the message
     * \return all the received messages
     */
    std::vector<std::vector<uint8_t>> &get_rx_msg(int radio_id);

protected:
    //!< \cond DOXY_IMPL
    modbus::sysc::Channel m_channel{"RS485Channel"};                                //!< The Channel modeling LoRa
    modbus::sysc::Transceiver m_rxtx0{"RXTX0"};                                     //!< The Transceiver 0
    modbus::sysc::Transceiver m_rxtx1{"RXTX1"};                                     //!< The Transceiver 1
    modbus::sysc::Transceiver m_rxtx2{"RXTX2"};                                     //!< The Transceiver 2
    modbus::UartCfg m_uart_cfg;                                                     //!< uart configuration to use
    std::vector<modbus::sysc::Transceiver *> m_rxtxs{&m_rxtx0, &m_rxtx1, &m_rxtx2}; //!< Vector of all radios
    std::map<modbus::TransceiverCallback::Event, int> m_map_events[RXTX_COUNT];     //!< Store events per Transceivers
    std::vector<std::vector<uint8_t>> m_rx_msg[RXTX_COUNT]; //!< Store receiced messages per Transceiver
    //!< \endcond
};

Test::Test(const sc_module_name &name)
    : sc_module(name)
{
    SC_THREAD(main);

    m_uart_cfg.set_baudrate(19200);

    for (int idx = 0; idx < RXTX_COUNT; ++idx)
    {
        m_rxtxs[idx]->set_id(idx);
        m_rxtxs[idx]->set_callback(this);
        m_rxtxs[idx]->set_uart_cfg(m_uart_cfg);
        m_channel.add(m_rxtxs[idx]);
    }
}

Test::~Test()
{
}

void Test::event(modbus::TransceiverBase *transceiver, Event evt)
{
    add_event(transceiver->get_id(), evt);
}

void Test::rx_done(modbus::TransceiverBase *transceiver, uint8_t *data, uint32_t size)
{
    add_receive_msg(transceiver->get_id(), data, size);
}

void Test::add_event(int radio_id, modbus::TransceiverCallback::Event evt)
{
    std::map<modbus::TransceiverCallback::Event, int>::iterator it;

    it = m_map_events[radio_id].find(evt);
    if (it == m_map_events[radio_id].end())
    {
        m_map_events[radio_id][evt] = 1;
        return;
    }
    it->second++;
}

int Test::get_event_count(int radio_id, modbus::TransceiverCallback::Event evt) const
{
    std::map<modbus::TransceiverCallback::Event, int>::const_iterator it;

    it = m_map_events[radio_id].find(evt);
    if (it == m_map_events[radio_id].end()) return 0;
    return it->second;
}

void Test::add_receive_msg(int radio_id, uint8_t *data, uint32_t size)
{
    std::vector<uint8_t> msg;

    msg.resize(size);
    memcpy(msg.data(), data, size);

    m_rx_msg[radio_id].push_back(msg);
}

std::vector<std::vector<uint8_t>> &Test::get_rx_msg(int radio_id)
{
    return m_rx_msg[radio_id];
}

void Test::main()
{
    std::vector<uint8_t> data0 = {0x01, 0x02, 0x03, 0x04};
    std::vector<uint8_t> data1 = {0x05, 0x06, 0x07, 0x08};

    // Step 1: test reception of event RX_TIMEOUT, when no LoRa message was received
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, modbus::TransceiverCallback::Event::RX_TIMEOUT), 0);

    m_rxtx0.rx(RX_WAIT_MS);

    wait(RX_CYCLE_MS, SC_MS);

    // Check the that Radio 0 did timeout
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, modbus::TransceiverCallback::Event::RX_TIMEOUT), 1);
    // And that Radio 0 didn't receive any message
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0).size(), 0);

    // Step 2: test reception of a LoRa message
    m_rxtx0.rx(RX_WAIT_MS);

    wait(30.0, SC_MS);

    // Make sure that the TX_DONE counter for Radio 1 is indeed 0
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, modbus::TransceiverCallback::Event::TX_DONE), 0);

    m_rxtx1.tx(data0.data(), (uint32_t)data0.size());

    wait(30.0, SC_MS); // Wait long enough to make sure the message was fully transmitted

    // Test that the Radio1 did receive the event TX_DONE
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, modbus::TransceiverCallback::Event::TX_DONE), 1);

    // Test if the receive message is identical to the message sent
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0).size(), 1);
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0)[0].size(), data0.size());
    ESYSTEST_REQUIRE_EQUAL(get_rx_msg(0)[0], data0);

    // Step 3: test the collision of 2 messages
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, modbus::TransceiverCallback::Event::RX_TIMEOUT), 1);

    // Radio 0 is set to receive a message
    m_rxtx0.rx(RX_WAIT_MS);

    wait(10.0, SC_MS);

    // The Radio 1 send a message
    m_rxtx1.tx(data0.data(), (uint32_t)data0.size());

    wait(1.0, SC_MS);

    // After a small delay, Radio 2 send a message with the exact same LoRa configuration
    m_rxtx2.tx(data1.data(), (uint32_t)data1.size());

    // We wait long enough to have the Radio 0 timing out of the message reception.
    wait(RX_CYCLE_MS, SC_MS);

    // Check that Radio 1 has indeed sent 2 messages so far
    ESYSTEST_REQUIRE_EQUAL(get_event_count(1, modbus::TransceiverCallback::Event::TX_DONE), 2);
    // Check that Radio 2 has just sent 1 message so far
    ESYSTEST_REQUIRE_EQUAL(get_event_count(2, modbus::TransceiverCallback::Event::TX_DONE), 1);
    // Check that since there was an interference between messages sent by Radio 1 and 2, Radio 0 did timeout once more
    ESYSTEST_REQUIRE_EQUAL(get_event_count(0, modbus::TransceiverCallback::Event::RX_TIMEOUT), 2);
}

/*! \class Channel01 esyslora_t/sim/channel01.cpp "esyslora_t/sim/channel01.cpp"
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(Channel01)
{
    simulation simul;

    Test test("Test");

    sc_simulation::init();

    int result = simul.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace channel01

} // namespace sysc

} // namespace modbus_t

} // namespace esys
