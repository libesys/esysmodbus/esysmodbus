/*!
 * \file esys/modbus_t/main.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#define ESYSTEST_TEST_MAIN

#include <esystest/unit_test.h>
#include "esys/modbus_t/testcasectrl.h"
#include <esystest/stdlogger.h>

#include <iostream>

class Main
{
public:
    Main();
    ~Main();

protected:
    esystest::StdLogger m_logger;
    esys::modbus_t::TestCaseCtrl m_test_ctrl;
};

Main::Main()
{
    m_logger.Set(&std::cout);
    esystest::Logger::Set(&m_logger);
    esystest::TestCaseCtrl::Set(&m_test_ctrl);
}

Main::~Main()
{
}

ESYSTEST_GLOBAL_FIXTURE(Main);
