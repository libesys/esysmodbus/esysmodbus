/*!
 * \file esys/modbus/uartcfg.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/uartcfg.h"

namespace esys
{

namespace modbus
{

UartCfg::UartCfg()
{
}

UartCfg::~UartCfg()
{
}

void UartCfg::set_baudrate(unsigned int baudrate)
{
    m_baudrate = baudrate;
}

unsigned int UartCfg::get_baudrate() const
{
    return m_baudrate;
}

void UartCfg::set_data_bit_count(DataBitCount data_bit_count)
{
    m_data_bit_count = data_bit_count;
}

UartCfg::DataBitCount UartCfg::get_data_bit_count() const
{
    return m_data_bit_count;
}

void UartCfg::set_parity(Parity parity)
{
    m_parity = parity;
}

UartCfg::Parity UartCfg::get_parity() const
{
    return m_parity;
}

void UartCfg::set_stop_bit(StopBit stop_bit)
{
    m_stop_bit = stop_bit;
}

UartCfg::StopBit UartCfg::get_stop_bit() const
{
    return m_stop_bit;
}

unsigned int UartCfg::get_symbol_bit_count() const
{
    unsigned int count;

    if (get_data_bit_count() == DataBitCount::Eight)
        count = 8;
    else
        count = 7;

    if (get_parity() != Parity::None) ++count;

    if (get_stop_bit() == StopBit::Two)
        count += 2;
    else
        ++count;

    ++count; // For the start bit
    return count;
}

} // namespace modbus

} // namespace esys

ESYSMODBUS_API std::ostream &operator<<(std::ostream &os, const esys::modbus::UartCfg *uart_cfg)
{
    return os;
}

ESYSMODBUS_API std::ostream &operator<<(std::ostream &os, const esys::modbus::UartCfg &uart_cfg)
{
    return os;
}
