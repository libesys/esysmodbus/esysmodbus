/*!
 * \file esys/modbus/sysc/channel_if_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/channel_if.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

Channel_if::Channel_if()
{
}

Channel_if::~Channel_if()
{
}

void Channel_if::set_cb_if(ChannelCb_if *cb_if)
{
    m_cb_if = cb_if;
}

ChannelCb_if *Channel_if::get_cb_if()
{
    return m_cb_if;
}

} // namespace sysc

} // namespace modbus

} // namespace esys
