/*!
 * \file esys/modbus/sysc/msg_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/msg.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

int64_t Msg::m_count = 0;

Msg::Msg()
{
    m_id = m_count;
    ++m_count;
}

Msg::~Msg()
{
}

//! Set the uart HW configuration used for this message
/*!
 * \param[in] uarrt_cfg the uart HW configuration
 */
void Msg::set_uart_cfg(const UartCfg &uart_cfg)
{
    m_uart_cfg = uart_cfg;
}

//! Get the uart HW configuration used for this message
/*!
 * \result the uart HW configuration
 */
const UartCfg &Msg::get_uart_cfg() const
{
    return m_uart_cfg;
}

//! Get the uart HW configuration used for this message
/*!
 * \result the uart HW configuration
 */
UartCfg &Msg::get_uart_cfg()
{
    return m_uart_cfg;
}

void Msg::set_data(const std::vector<uint8_t> &data)
{
    m_data = data;
}

const std::vector<uint8_t> &Msg::get_data() const
{
    return m_data;
}

std::vector<uint8_t> &Msg::get_data()
{
    return m_data;
}

void Msg::set_size(std::size_t size)
{
    m_data.resize(size);
}

std::size_t Msg::get_size() const
{
    return m_data.size();
}

void Msg::set_sent_time(const sc_time &sent_time)
{
    m_sent_time = sent_time;
}

const sc_time &Msg::get_sent_time() const
{
    return m_sent_time;
}

int64_t Msg::get_id() const
{
    return m_id;
}

} // namespace sysc

} // namespace modbus

} // namespace esys
