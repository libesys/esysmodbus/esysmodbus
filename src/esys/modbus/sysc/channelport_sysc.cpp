/*!
 * \file esys/modbus/sysc/channelport_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/channelport.h"
#include "esys/modbus/sysc/channelmsg.h"

#include <cassert>

namespace esys
{

namespace modbus
{

namespace sysc
{

ChannelPort::ChannelPort(const sc_module_name &name)
    : sc_module(name)
    , Msg_if()
{
}

ChannelPort::~ChannelPort()
{
}

void ChannelPort::tx_msg(std::shared_ptr<Msg> msg)
{
    std::shared_ptr<ChannelMsg> ch_msg = std::make_shared<ChannelMsg>();

    ch_msg->set_msg(msg);
    ch_msg->set_src_port_id(get_id());

    if (get_channel_if() == nullptr) return;

    get_channel_if()->ch_tx_msg(ch_msg);
}

void ChannelPort::rx(const UartCfg &uart_cfg)
{
    m_uart_cfg = &uart_cfg;
}

void ChannelPort::idle()
{
    m_uart_cfg = nullptr;
}

void ChannelPort::chcb_msg_start(std::shared_ptr<ChannelMsg> msg)
{
    if (get_uart_cfg() == nullptr) return;

    if (get_cur_msg() == nullptr)
    {
        // No interference
        set_cur_msg(msg);
        return;
    }

    // There are intereferences. For now, assumes that the message is lost
    set_cur_msg(nullptr);
}

void ChannelPort::chcb_msg_done(std::shared_ptr<ChannelMsg> msg)
{
    if (get_cur_msg() == nullptr) return;

    assert(get_cur_msg()->get_id() == msg->get_id());

    if (get_msg_cb_if() == nullptr) return;

    get_msg_cb_if()->rx_msg(msg->get_msg());

    // No pending message.
    set_cur_msg(nullptr);
}

void ChannelPort::set_id(std::size_t id)
{
    m_id = id;
}

std::size_t ChannelPort::get_id() const
{
    return m_id;
}

void ChannelPort::set_channel_if(Channel_if *channel_if)
{
    m_channel_if = channel_if;
}

Channel_if *ChannelPort::get_channel_if() const
{
    return m_channel_if;
}

void ChannelPort::set_msg_cb_if(MsgCb_if *msg_rx_if)
{
    m_msg_cb_if = msg_rx_if;
}

MsgCb_if *ChannelPort::get_msg_cb_if() const
{
    return m_msg_cb_if;
}

void ChannelPort::set_cur_msg(std::shared_ptr<ChannelMsg> cur_msg)
{
    m_cur_msg = cur_msg;
}

std::shared_ptr<ChannelMsg> ChannelPort::get_cur_msg()
{
    return m_cur_msg;
}

const std::shared_ptr<ChannelMsg> ChannelPort::get_cur_msg() const
{
    return m_cur_msg;
}

const UartCfg *ChannelPort::get_uart_cfg()
{
    return m_uart_cfg;
}

} // namespace sysc

} // namespace modbus

} // namespace esys
