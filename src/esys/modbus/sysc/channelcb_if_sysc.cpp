/*!
 * \file esys/modbus/sysc/channelcb_if_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/channelcb_if.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

ChannelCb_if::ChannelCb_if()
{
}

ChannelCb_if::~ChannelCb_if()
{
}

} // namespace sysc

} // namespace modbus

} // namespace esys
