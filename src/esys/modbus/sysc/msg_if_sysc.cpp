/*!
 * \file esys/modbus/sysc/msg_if_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/msg_if.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

Msg_if::Msg_if()
{
}

Msg_if::~Msg_if()
{
}

void Msg_if::set_cb_if(MsgCb_if *cb_if)
{
    m_cb_if = cb_if;
}

MsgCb_if *Msg_if::get_cb_if()
{
    return m_cb_if;
}

} // namespace sysc

} // namespace modbus

} // namespace esys
