/*!
 * \file esys/modbus/sysc/msgcb_if_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/msgcb_if.h"

#include <memory>

namespace esys
{

namespace modbus
{

namespace sysc
{

MsgCb_if::MsgCb_if()
{
}

MsgCb_if::~MsgCb_if()
{
}

} // namespace sysc

} // namespace modbus

} // namespace esys
