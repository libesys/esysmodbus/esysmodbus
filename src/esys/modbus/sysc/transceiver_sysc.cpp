/*!
 * \file esys/modbus/sysc/transceiver_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/transceiver.h"

#include <cassert>

namespace esys
{

namespace modbus
{

namespace sysc
{

Transceiver::Transceiver()
    : sc_module()
    , MsgCb_if()
    , TransceiverBase()
{
    SC_THREAD(main);
}

Transceiver::Transceiver(const sc_module_name &name)
    : sc_module(name)
    , MsgCb_if()
    , TransceiverBase()
{
    SC_THREAD(main);
}

Transceiver::~Transceiver()
{
}

void Transceiver::set_msg_if(Msg_if *msg_if)
{
    m_msg_if = msg_if;
}

Msg_if *Transceiver::get_msg_if()
{
    return m_msg_if;
}

int Transceiver::set_uart_cfg(const UartCfg &uart_cfg)
{
    m_uart_cfg = uart_cfg;

    return 0;
}

const UartCfg &Transceiver::get_uart_cfg() const
{
    return m_uart_cfg;
}

UartCfg &Transceiver::get_uart_cfg()
{
    return m_uart_cfg;
}

Transceiver::Status Transceiver::get_status() const
{
    return m_status;
}

int Transceiver::tx(uint8_t *packet, uint32_t size)
{
    if (get_status() != Status::IDLE) return -1;

    std::shared_ptr<Msg> msg = std::make_shared<Msg>();
    std::vector<uint8_t> data;

    data.resize(size);
    memcpy(data.data(), packet, size);

    msg->set_size(size);
    msg->set_data(data);
    msg->set_uart_cfg(get_uart_cfg());

    m_tx_msg = msg;
    m_next_internal_state_evt.cancel();
    m_next_internal_state_evt.notify(m_idle_to_tx);
    set_internal_state(InternalState::GOING_TO_TX);
    return 0;
}

int Transceiver::rx(uint32_t timeout)
{
    if (get_status() != Status::IDLE) return -1;

    set_rx_timed_out(false);

    m_rx_timeout = sc_time(timeout, SC_MS);
    set_status(Status::RX_RUNNING);
    m_next_internal_state_evt.notify_delayed();

    auto msg_if = get_msg_if();
    if (msg_if == nullptr) return -2;

    msg_if->rx(get_uart_cfg());
    return 0;
}

void Transceiver::set_internal_state(InternalState internal_state)
{
    m_internal_state = internal_state;
}

Transceiver::InternalState Transceiver::get_internal_state()
{
    return m_internal_state;
}

void Transceiver::set_rx_timed_out(bool rx_timed_out)
{
    m_rx_timed_out = rx_timed_out;
}

bool Transceiver::get_rx_timed_out() const
{
    return m_rx_timed_out;
}

void Transceiver::set_status(Status status)
{
    m_status = status;
}

void Transceiver::rx_msg(std::shared_ptr<Msg> msg)
{
    if (get_status() != Status::RX_RUNNING) return;

    m_next_internal_state_evt.cancel();

    m_rx_msg = msg;

    set_internal_state(InternalState::GOING_TO_IDLE);
    m_next_internal_state_evt.notify_delayed(m_rx_to_idle);

    if (get_callback() == nullptr) return;

    get_callback()->rx_done(this, m_rx_msg->get_data().data(), m_rx_msg->get_data().size());
}

void Transceiver::tx_done()
{
    m_next_internal_state_evt.cancel();
    m_next_internal_state_evt.notify(m_tx_to_idle);
    set_internal_state(InternalState::GOING_TX_TO_IDLE);
}

void Transceiver::main()
{
    while (true)
    {
        wait(m_next_internal_state_evt);

        switch (get_internal_state())
        {
            case InternalState::IDLE:
                if (get_status() == Status::RX_RUNNING)
                {
                    set_internal_state(InternalState::GOING_TO_RX);
                    m_next_internal_state_evt.notify_delayed(m_idle_to_rx);
                }
                break;
            case InternalState::GOING_TO_IDLE:
                set_internal_state(InternalState::IDLE);
                set_status(Status::IDLE);
                if (get_rx_timed_out() && (get_callback() != nullptr))
                    get_callback()->event(this, TransceiverCallback::Event::RX_TIMEOUT);
                break;
            case InternalState::GOING_TO_RX:
                set_internal_state(InternalState::RX);
                m_next_internal_state_evt.notify_delayed(m_rx_timeout);
                break;
            case InternalState::RX:
                set_internal_state(InternalState::GOING_TO_IDLE);
                m_next_internal_state_evt.notify_delayed(m_rx_to_idle);
                set_rx_timed_out(true);
                break;
            case InternalState::GOING_TO_TX:
                set_internal_state(InternalState::TX);
                if (get_msg_if() != nullptr) get_msg_if()->tx_msg(m_tx_msg);
                break;
            case InternalState::GOING_TX_TO_IDLE:
                set_internal_state(InternalState::IDLE);
                set_status(Status::IDLE);
                if (get_callback() != nullptr) get_callback()->event(this, TransceiverCallback::Event::TX_DONE);
                break;
            default:;
        }
    }
}

} // namespace sysc

} // namespace modbus

} // namespace esys
