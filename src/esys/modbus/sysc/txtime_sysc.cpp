/*!
 * \file esys/modbus/sysc/txtime_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/txtime.h"

#include <cassert>

namespace esys
{

namespace modbus
{

namespace sysc
{

TxTime::TxTime()
    : modbus::TxTime()
{
}

TxTime::TxTime(Msg *msg)
    : modbus::TxTime()
    , m_msg(msg)
{
}

TxTime::~TxTime()
{
}

void TxTime::set_msg(Msg *msg)
{
    m_msg = msg;
}

Msg *TxTime::get_msg() const
{
    return m_msg;
}

double TxTime::get_msg_time()
{
    assert(get_msg() != nullptr);

    set_uart_cfg(&get_msg()->get_uart_cfg());

    return get_msg_time(get_msg()->get_size());
}

} // namespace sysc

} // namespace modbus

} // namespace esys
