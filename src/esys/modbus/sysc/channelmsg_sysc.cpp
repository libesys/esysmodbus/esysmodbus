/*!
 * \file esys/modbus/sysc/channelmsg_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/sysc/channelmsg.h"

namespace esys
{

namespace modbus
{

namespace sysc
{

int64_t ChannelMsg::m_count = 0;

ChannelMsg::ChannelMsg()
{
    m_id = m_count;
    ++m_count;
}

ChannelMsg::~ChannelMsg()
{
}

void ChannelMsg::set_msg(std::shared_ptr<Msg> msg)
{
    m_msg = msg;
}

std::shared_ptr<Msg> ChannelMsg::get_msg()
{
    return m_msg;
}

void ChannelMsg::set_src_port_id(std::size_t src_port_id)
{
    m_src_port_id = src_port_id;
}

std::size_t ChannelMsg::get_src_port_id() const
{
    return m_src_port_id;
}

void ChannelMsg::set_start_time(const sc_time &start_time)
{
    m_start_time = start_time;
}

const sc_time &ChannelMsg::get_start_time() const
{
    return m_start_time;
}

sc_time &ChannelMsg::get_start_time()
{
    return m_start_time;
}

void ChannelMsg::set_end_time(const sc_time &end_time)
{
    m_end_time = end_time;
}

const sc_time &ChannelMsg::get_end_time() const
{
    return m_end_time;
}

sc_time &ChannelMsg::get_end_time()
{
    return m_end_time;
}

int64_t ChannelMsg::get_id() const
{
    return m_id;
}

} // namespace sysc

} // namespace modbus

} // namespace esys
