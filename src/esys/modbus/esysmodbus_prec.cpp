/*!
 * \file esys/modbus/esysmodbus_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"

// TODO: reference any additional headers you need in esysmodbus_prec.h
// and not in this file
