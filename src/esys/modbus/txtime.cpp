/*!
 * \file esys/modbus/txtime.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/txtime.h"

#include <cassert>

namespace esys
{

namespace modbus
{

TxTime::TxTime()
{
}

TxTime::TxTime(UartCfg *uart_cfg)
    : m_uart_cfg(uart_cfg)
{
}

TxTime::~TxTime()
{
}

void TxTime::set_uart_cfg(UartCfg *uart_cfg)
{
    m_uart_cfg = uart_cfg;
}

UartCfg *TxTime::get_uart_cfg() const
{
    return m_uart_cfg;
}

double TxTime::get_msg_time(int msg_size)
{
    assert(get_uart_cfg() != nullptr);

    double t = (msg_size * get_uart_cfg()->get_symbol_bit_count());
    t = t / get_uart_cfg()->get_baudrate();
    return t;
}

} // namespace modbus

} // namespace esys
