/*!
 * \file esys/modbus/transceiverbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/transceiverbase.h"

namespace esys
{

namespace modbus
{

TransceiverBase::TransceiverBase()
{
}

TransceiverBase::~TransceiverBase()
{
}

void TransceiverBase::set_callback(TransceiverCallback *callback)
{
    m_callback = callback;
}

TransceiverCallback *TransceiverBase::get_callback() const
{
    return m_callback;
}

void TransceiverBase::set_id(int id)
{
    m_id = id;
}

int TransceiverBase::get_id() const
{
    return m_id;
}

} // namespace modbus

} // namespace esys
