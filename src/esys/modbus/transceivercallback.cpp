/*!
 * \file esys/modbus/transceivercallback.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/modbus/esysmodbus_prec.h"
#include "esys/modbus/transceivercallback.h"

namespace esys
{

namespace modbus
{

TransceiverCallback::TransceiverCallback()
{
}
TransceiverCallback::~TransceiverCallback()
{
}

} // namespace modbus

} // namespace esys
